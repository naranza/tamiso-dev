<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

declare(strict_types=1);
use bateo_test as test;

class bateo_testcase implements bateo_testcase_interface
{

  public function setup()
  {
    require_once TAMISO_DIR . '/url.php';
  }

  public function teardown()
  {

  }

  public function t_moa(test $t)
  {
    $t->wie = [
      'filter/trim' => ['function' => 'trim'],
      'filter/intval' => ['function' => 'intval']
    ];
    $t->wig = fongo_read_filter(['filter/trim', 'filter/intval']);
    $t->pass_if($t->wie === $t->wig);
  }

}