<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

$root_dir = realpath(__DIR__ . '/..');

require $root_dir . '/src/initme.php';

define('TAMISO_TEST_DIR', $root_dir . '/test');

const TAMISO_TEST_DATA_DIR = TAMISO_TEST_DIR . '/data';
const TAMISO_TEST_FUNC_DIR = TAMISO_TEST_DIR . '/func';
