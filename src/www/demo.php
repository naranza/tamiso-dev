<!DOCTYPE html>
<html>
  <head>
    <title>TODO supply a title</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/toco-core.css">
    <link rel="stylesheet" href="/css/demo.css">
  </head>
  <body>

    <h3>Form with auto</h3>
    <fieldset class="toco-row">
      <legend>test</legend>
      <?php $class = 'toco-auto' ?>
      <?php for ($i = 1; $i <= 8; $i++): ?>
        <div class="form-cell <?= $class ?>">
          <label><?= $class ?></label>
          <input type = "text">
        </div>
      <?php endfor ?>
    </fieldset>

    <h3>Form Grid fluid width</h3>
    <fieldset class="toco-row">
      <legend>test</legend>
      <?php $class = 'toco-2-12' ?>
      <div class="form-cell <?= $class ?>">
        <label><?= $class ?></label>
        <input type = "text">
      </div>
      <?php $class = 'toco-4-12' ?>
      <div class="form-cell <?= $class ?>">
        <label><?= $class ?></label>
        <input type = "text">
      </div>
      <?php $class = 'toco-3-12' ?>
      <div class="form-cell <?= $class ?>">
        <label><?= $class ?></label>
        <input type = "text">
      </div>
      <?php $class = 'toco-3-12' ?>
      <div class="form-cell <?= $class ?>">
        <label><?= $class ?></label>
        <input type = "text">
      </div>
    </fieldset>


    <h3>Grid fixed width 2</h3>
    <?php for ($i = 1; $i < 2; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-sm-' . $i . '-2' ?>
        <?php $class2 = 'toco-sm-' . (2 - $i) . '-2' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>
    <h3>Grid fixed width 3</h3>
    <?php for ($i = 1; $i < 3; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-sm-' . $i . '-3' ?>
        <?php $class2 = 'toco-sm-' . (3 - $i) . '-3' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>
    <h3>Grid fixed width 4</h3>
    <?php for ($i = 1; $i < 4; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-sm-' . $i . '-4' ?>
        <?php $class2 = 'toco-sm-' . (4 - $i) . '-4' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>

    <h3>Grid break</h3>
    <div class="toco-row">
      <?php $class1 = 'toco-auto' ?>
      <div class="grid toco-auto"><?= $class1 ?></div>
      <div class="grid toco-auto"><?= $class1 ?></div>
      <div class="toco-break"><?= 'toco-break' ?></div>
      <div class="grid toco-auto"><?= $class1 ?></div>
      <div class="grid toco-auto"><?= $class1 ?></div>
      <div class="grid toco-auto"><?= $class1 ?></div>
    </div>
    <h3>Grid fluid width auto long content</h3>
    <div class="toco-row">
      <?php for ($i = 1; $i <= 7; $i++): ?>
        <?php $class1 = 'toco-auto' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
      <?php endfor ?>
    </div>
    <h3>Grid fluid width auto short content</h3>
    <div class="toco-row">
      <?php for ($i = 1; $i <= 7; $i++): ?>
        <?php $class1 = 'toco-auto' ?>
        <div class="grid <?= $class1 ?>"><?= $i ?></div>
      <?php endfor ?>
    </div>
    <h3>Grid fluid width 2</h3>
    <?php for ($i = 1; $i < 2; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-' . $i . '-2' ?>
        <?php $class2 = 'toco-' . (2 - $i) . '-2' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>
    <h3>Grid fluid width 3</h3>
    <?php for ($i = 1; $i < 3; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-' . $i . '-3' ?>
        <?php $class2 = 'toco-' . (3 - $i) . '-3' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>
    <h3>Grid fluid width 4</h3>
    <?php for ($i = 1; $i < 4; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-' . $i . '-4' ?>
        <?php $class2 = 'toco-' . (4 - $i) . '-4' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>
    <h3>Grid fluid width 12</h3>
    <?php for ($i = 1; $i < 12; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-' . $i . '-12' ?>
        <?php $class2 = 'toco-' . (12 - $i) . '-12' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>
    <h3>Grid fluid width 24</h3>
    <?php for ($i = 1; $i < 24; $i++): ?>
      <div class="toco-row">
        <?php $class1 = 'toco-' . $i . '-24' ?>
        <?php $class2 = 'toco-' . (24 - $i) . '-24' ?>
        <div class="grid <?= $class1 ?>"><?= $class1 ?></div>
        <div class="grid <?= $class2 ?>"><?= $class2 ?></div>
      </div>
    <?php endfor ?>


  </body>
</html>
