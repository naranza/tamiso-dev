<?php

declare(strict_types=1);

ini_set('display_errors', 'true');
ini_set('track_errors', 'true');
ini_set('display_startup_errors', 'true');
error_reporting(E_ALL);

$root_dir = realpath(__DIR__ . '/../../../..');
//print_r($root_dir);
//die;
$env = (false !== strpos($_SERVER['SERVER_NAME'], '.lan')) ? 'dev' : 'pro';
require $root_dir . '/lib/sesto/bootstrap/sys.php';
require $root_dir . '/lib/sesto/bootstrap/app.php';
$sys_dirs = ['tamiso', 'sesto'];
$error_sys = sesto_bootstrap_sys($root_dir, $env, $sys_dirs);
$error_app = sesto_bootstrap_app(SYS_APP_DIR, 'tamiso', ['tamiso']);
//print_r(get_defined_constants(true)['user']);
//die;
if ('' === $error_sys && '' === $error_app) {
  require TAMISO_DIR . '/engine.php';
  require SESTO_DIR . '/system/error_handler_web.php';
  require SESTO_DIR . '/app/call.php';
//  require BELLA_CMS_DIR . '/routing/default.php';
  $exit_code = sesto_app_call(
    'tamiso_engine',
    [APP_CONF_DIR . '/app'],
    'sesto_system_error_handler_web'
  );
} else {
  $exit_code = 1;
}

exit($exit_code);
