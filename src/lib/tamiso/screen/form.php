<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_screen_form(array $screen, array $record, array $validation, string $action): array
{
  /* escape */
//  $screen['elements'] = tamiso_screen_escape($screen, $record);

  /* form */
  $built = $screen['form']['attribs'] ?? [];
  $built['attribs'] = array_merge(
    ['id' => 't-context-form', 'class' => "t-form", 'action' => $action, 'method' => 'post'],
    $screen['form']['attribs'] ?? []
  );

  /* fieldsets */
  foreach ($screen['form']['fieldsets'] as $fieldset_id) {
    $built['fieldsets'][$fieldset_id] = $screen['fieldsets'][$fieldset_id] ?? [];
    $rows = $built['fieldsets'][$fieldset_id]['rows'];
    $built['fieldsets'][$fieldset_id]['rows'] = [];
    foreach ($rows as $row_id) {
      $built['fieldsets'][$fieldset_id]['rows'][$row_id] = $screen['rows'][$row_id] ?? [];
      $cells = $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'];
      $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'] = [];
//
      $built['fieldsets'][$fieldset_id]['rows'][$row_id]['attribs'] = array_merge(
        ['id' => 't-row-' . $row_id, 'class' => "toco-row t_form_row gut"],
        $built['fieldsets'][$fieldset_id]['rows'][$row_id]['attribs'] ?? []
      );
      foreach ($cells as $cell_id) {
        $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id] = $screen['cells'][$cell_id] ?? [];
        $elements = $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'];
        $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'] = [];
        $cell_size = $screen['cells'][$cell_id]['size'] ?? 'auto';
        $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['validation'] = '';
        $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['attribs'] = array_merge(
          ['id' => 't-cell-' . $cell_id, 'class' => 't-form-cell toco-' . $cell_size],
          $screen['cells'][$cell_id]['attribs'] ?? []
        );
        if(isset($validation[$cell_id])) {
          $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['validation'] = $validation[$cell_id];
          $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['attribs']['class'] .= ' t-form-cell-error';
        }
        foreach ($elements as $field_name) {
          $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'][$field_name] = $screen['elements'][$field_name] ?? [];
          $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'][$field_name]['tag'] = $screen['elements'][$field_name]['tag'] ?? 'input';
          if ('textarea' === $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'][$field_name]['tag']) {
            $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'][$field_name]['attribs'] = array_merge(
              [
                'id' => 't-element-' . $field_name,
                'name' => $field_name,
              ],
              $screen['elements'][$field_name]['attribs'] ?? []
            );
            $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'][$field_name]['content'] =  $record[$field_name] ?? '';
          } else {
            $built['fieldsets'][$fieldset_id]['rows'][$row_id]['cells'][$cell_id]['elements'][$field_name]['attribs'] = array_merge(
              [
                'id' => 't-element-' . $field_name,
                'name' => $field_name,
                'value' => $record[$field_name] ?? '',
              ],
              $screen['elements'][$field_name]['attribs'] ?? []
            );

          }
        }
      }
    }
  }
  foreach ($screen['hiddens'] as $field_name) {
    $built['hiddens'][$field_name]['tag'] = 'input';
    $built['hiddens'][$field_name]['attribs'] = array_merge(
      [
        'id' => 't-element-' . $field_name,
        'type' => 'hidden',
        'name' => $field_name,
        'value' => $record[$field_name] ?? '',
      ],
      $screen['elements'][$field_name]['attribs'] ?? []
    );
  }

  return $built;
}
