<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/read/dict.php';

function tamiso_read_dict(array $app): array
{
  /* load dictionary */
  $dicts = [];
  if (true === $app['procedure']['auto_dict']) {
    $dicts[] = 'action::_dict';
    $dicts[] = 'tamiso::dict/csrf';
  }
  return fongo_read_dict($dicts);
}
