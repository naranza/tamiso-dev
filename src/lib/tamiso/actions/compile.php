<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/actions/struct.php';

function tamiso_actions_compile(array $app, array $actions): array
{
  $compiled = [];
  foreach ($actions as $name => $action) {
    $compiled_action = array_merge(tamiso_actions_struct(), $action);
    $compiled_action['name'] = strtolower(trim($name));
    switch ($compiled_action['name']) {
      case 'create':
        $compiled_action['type'] = 'goto';
        $compiled_action['caption'] = 'Create';
        $compiled_action['target_route'] = ',,create';
        break;
      case 'read':
        $compiled_action['type'] = 'data';
        $compiled_action['caption'] = 'Read';
        $compiled_action['target_route'] = ',,read';
        break;
      case 'update':
        $compiled_action['type'] = 'data';
        $compiled_action['caption'] = 'Update';
        $compiled_action['target_route'] = ',,update';
        break;
      case 'delete':
        $compiled_action['type'] = 'data';
        $compiled_action['caption'] = 'Delete';
        $compiled_action['target_route'] = ',,delete';
        break;
//      case 'filter':
//        $compiled_action['content'] = '<span class="fas fa-filter"></span> Filter';
//        $compiled_action['target_route'] = 'tamiso,filter,set';
//        break;
      default:
        $compiled_action['type'] = (string) ($action['type'] ?? 'data');
        $compiled_action['caption'] = $action['caption'] ?? $compiled_action['name'];
        $compiled_action['target_route'] = $action['target_route'] ?? ',,,';

        break;
    }
    $compiled[$name] = $compiled_action;
  }
  return $compiled;
}
