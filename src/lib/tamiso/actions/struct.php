<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_actions_struct(): array
{
  return [
    'name' => '',
    'type' => '',
    'icon' => '',
    'caption' => '',
    'set_family' => false,
    'family_type' => '',
    'family_name' => '',
    'target_route' => ''
  ];
}
