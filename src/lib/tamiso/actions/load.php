<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/actions/read.php';
require_once TAMISO_DIR . '/actions/compile.php';

function tamiso_actions_load(array $app, string $action_dir): array
{
  return tamiso_actions_compile($app, tamiso_actions_read($action_dir));
}
