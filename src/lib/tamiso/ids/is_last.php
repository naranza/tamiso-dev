<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_ids_is_last(array $ids): bool
{
  return $ids['pos'] > ($ids['size'] - 1);
}
