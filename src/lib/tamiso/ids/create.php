<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/struct/ids.php';

function tamiso_ids_create(array $data, int $pos): array
{
  return [
    'size' => count($data),
    'data' => $data,
    'pos' => $pos,
    'id_value' => $data[$pos] ?? null
  ];
}
