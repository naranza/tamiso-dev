<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_ids_next(array &$ids): void
{
  $ids['pos']++;
  $ids['id_value'] = $ids['data'][$ids['pos']] ?? null;
//  if ($ids['pos'] <= ($ids['size'] - 1)) {
//    $result = true;
//  } else {
//    $result = false;
//  }
//  return $result;
}
