<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/module/read.php';
require_once TAMISO_DIR . '/module/compile.php';

function tamiso_module_load(array $app, string $object_dir): array
{
  return tamiso_module_compile($app, tamiso_module_read($object_dir));
}
