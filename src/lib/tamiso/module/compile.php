<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);


function tamiso_module_compile(array $app, array $object): array
{
//  $compiled = [];
  $compiled = $object;
  $compiled['caption'] = $object['caption'] ?? '<module>';
  $compiled['family_name'] = $object['family_name'] ?? '';
  $compiled['family_level'] = $object['family_level'] ?? -1;
  $compiled['family_parent_level'] = $compiled['family_level'] - 1;
  $compiled['family_caption_field'] = $compiled['family_caption_field'] ?? '';
  return $compiled;
}
