<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/hook/attach.php';

function tamiso_attach(string $name, callable $callback, int $priority = 50): void
{
  global $tamiso_hooks;
  sesto_hook_attach($tamiso_hooks, $name, $callback, $priority);
}
