<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

const TAMISO_DIR = __DIR__;
const TAMISO_RELEASE = '2022.1';
const TAMISO_CODENAME = '';
const TAMISO_ROUTINE_DIR = __DIR__ . '/routine';
const TAMISO_VIEW_DIR = __DIR__ . '/view';
const TAMISO_MODULE_DIR = __DIR__ . '/module';
const TAMISO_QUERY_HASH_ALGO = 'tiger192,4';

/* init globals */
global $tamiso_hooks;
$tamiso_hooks = [];
$tamiso_route = [];