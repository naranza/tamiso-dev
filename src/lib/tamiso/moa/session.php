<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

function tamiso_moa_session(array $moa): array
{
  return [
    'm' => $moa['module'],
    'mo' => $moa['module'] . '/' . $moa['object'],
    'moa' => $moa['module'] . '/' . $moa['object'] . '/' . $moa['action']
  ];
}
