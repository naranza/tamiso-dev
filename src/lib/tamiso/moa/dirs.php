<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

function tamiso_moa_dirs(string $module_dir, string $object, string $action): array
{
  $module_procedure_dir = $module_dir . '/procedure';
  $object_procedure_dir = $module_procedure_dir . '/' . $object;
  $action_procedure_dir = $object_procedure_dir . '/' . $action;
  return [$module_procedure_dir, $object_procedure_dir, $action_procedure_dir];
}
