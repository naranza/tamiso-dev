<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

function tamiso_moa_from_path(string $path): array
{
  $parts = explode('/', ltrim($path, '/'));
  $moa = [
    'module' => $parts[0] ?? '',
    'object' => $parts[1] ?? '',
    'action' => $parts[2] ?? '',
  ];
  $moa['mo'] = implode(',', [$moa['module'], $moa['object']]);
  $moa['oa'] = implode(',', [$moa['object'], $moa['action']]);
  $moa['moa'] = implode(',', [$moa['module'], $moa['object'], $moa['action']]);
  $moa['/mo'] = '/' . $moa['module'] . '/' . $moa['object'];
  $moa['/moa'] = $moa['/mo'] . '/' . $moa['action'];
  return $moa;
}
