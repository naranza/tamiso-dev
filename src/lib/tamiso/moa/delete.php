<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_moa_delete(array $app): void
{
  if (isset($app['route']['/moa'])) {
    unset($_SESSION['tamiso'][$app['route']['/moa']]);
  }
}
