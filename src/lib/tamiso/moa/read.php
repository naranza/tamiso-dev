<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_moa_read(array $app): array
{
  if (isset($app['route']['/moa'])) {
    $return = $_SESSION['tamiso'][$app['route']['/moa']] ?? [];
  } else {
    $return = [];
  }
  return $return;
}
