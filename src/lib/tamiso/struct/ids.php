<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_struct_ids(): array
{
  return [
    'size' => 0,
    'data' => [],
    'pos' => 0,
    'id_value' => null
  ];
}
