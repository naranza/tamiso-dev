<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/moa/dirs.php';

function tamiso_family_compile_old(array &$family, array $app)
{
  foreach ($family as $id => $member) {
//    sesto_d($id, '$id');
//    sesto_d($member, '$member');
//    sesto_d($member['ids'][$member['pos']], '$member');
//    die;
    $moa = tamiso_moa_from_path($member['url']);
//    sesto_dump($moa, 'moa');

    /* load caller object config data */
    list(
      $caller_module_dir,
      $caller_object_dir,
      $caller_action_dir,
      $error
      ) = tamiso_moa_dirs($app, $moa['module'], $moa['object'], $moa['action']);

    /* load object config */
    $object = sesto_config_read($caller_object_dir . '/object');
//    $family[$id]['object_caption'] = $object['caption'];

    /* get record */

    $path = $caller_object_dir . '/_record.php';
    if (is_file($path) && is_readable($path)) {
      $function = include $path;
      $family[$id]['record'] = $function($member['ids'][$member['pos']]);
      break;
    } else {
      sesto_d('unable to get partent ' . $path);
    }
  }
}
