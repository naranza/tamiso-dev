<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_family_read(array $app): array
{
  $family = [];
  $family['tree'] = $_SESSION['tamiso']['family'][$app['object']['family_name']] ?? [];
  $family['parent_id'] = $family['tree'][$app['object']['family_parent_level']]['id_value'] ?? null;
  return $family;
}
