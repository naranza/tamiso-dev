<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_family_add(string $family_name, int $level, array $data): void
{
  $_SESSION['tamiso']['family'][$family_name][$level] = $data;
}
