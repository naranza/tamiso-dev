<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

require_once SESTO_DIR . '/route/resolve.php';
require_once TAMISO_DIR . '/moa/from_path.php';

function tamiso_route_resolve(string $url_base = null, string $url_path = null): array
{
  $route = sesto_route_resolve($url_base, $url_path);
  return array_merge($route, tamiso_moa_from_path($route['url_relative']));
}
