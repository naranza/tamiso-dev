<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_object_struct(): array
{
  return [
    'name' => '',
    'type' => '',
    'icon' => '',
    'caption' => '',
    'family_type' => '',
    'family_name' => ''
  ];
}
