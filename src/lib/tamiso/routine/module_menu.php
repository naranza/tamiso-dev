<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/view/render.php';
require_once SESTO_DIR . '/html/build.php';
require_once SESTO_DIR . '/html/element.php';
require_once SESTO_DIR . '/html/attribs.php';
require_once TAMISO_DIR . '/read/actions.php';
require_once TAMISO_DIR . '/default/views.php';
require_once TAMISO_DIR . '/default/viewdata.php';
require_once TAMISO_DIR . '/bcrumb/write.php';

class tamiso_routine_module_menu
{

  public bool $check_auth = true;

  final public function run(array $app)
  {
    /* init */
    $screen = sesto_config_read($app['procedure']['action_dir'] . '/_screen');

    /* viewdata */
    $viewdata = tamiso_default_viewdata($app);

    $viewdata['menu'] = [];
    foreach ($screen as $col_num => $col) {
      foreach ($col as $pos => $item) {
        $compiled_url = tamiso_url($app['route'], $item['target_route'], $item['target_params'] ?? []);
        $attribs = [
          'id' => 't-menu-item-' . $col_num . '-' . $pos,
          'class' => 'toco-1',
          'href' => $compiled_url
        ];
        $viewdata['menu'][$col_num][$pos] = sesto_html_element('a', $attribs, $item['label'] ?? '--');
      }
    }
//    for($i = 0; $i <= 12; $i++) {
//      $menu[0][$i + 1] = 'Menu Item A-'.$i;
//      $menu[1][$i + 1 + 13] = 'Menu Item B-'.$i;
//      $menu[2][$i + 1 + 13 + 13] = 'Menu Item C-'.$i;
//    }
//    $viewdata['form_action'] = tamiso_url($app['route'], '/tamiso/ids/select');
//    $viewdata['screen'] = $t_screen;
//    $viewdata['alert'] = $alert;
//    $viewdata['target'] = '';
//    $viewdata['t_type'] = '';
//    $viewdata['records'] = $app['records'];
//    $viewdata['t_parent'] = $app['t_parent'] ?? null;
//    $viewdata['t_grandparent'] = $app['t_grandparent'] ?? null;
//    $viewdata['actions'] = [];
//    /* add cancel button */
//    foreach ($app['action']['actions'] as $type => $actions) {
//      foreach ($actions as $name => $action) {
//        $compiled_url = tamiso_url($app['route'], $action['target'], $action['target_params']);
//        $attribs = [
//          'id' => 't_btn_' . $name,
//          'name' => 'caller',
//          'name' => 't_btn_' . $name,
//          'class' => 'toco-btn toco-btn-primary',
//          'value' => $compiled_url
//        ];
//        if ('header' === $type) {
//          $attribs['class'] .= ' t-btn-goto';
//        } else {
//          $attribs['class'] .= ' t-btn-context';
//          $attribs['form'] = 't-context-form';
//        }
//        $viewdata['actions'][$type][$name] = sesto_html_element('button', $attribs, $action['label']);
//      }
//    }

    /* views */
    $views = tamiso_default_views($app);
    $views['page_content'] = TAMISO_VIEW_DIR . '/module_menu.phtml';
//    $views['list_header'] = TAMISO_VIEW_DIR . '/list_header_button_bar.phtml';

    /* render */
    sesto_view_render($views, 'layout', $viewdata);
  }

}
