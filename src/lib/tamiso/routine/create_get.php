<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/view/render.php';
require_once SESTO_DIR . '/html/element.php';
require_once SESTO_DIR . '/html/build.php';
require_once TAMISO_DIR . '/moa/read.php';
require_once TAMISO_DIR . '/moa/write.php';
require_once TAMISO_DIR . '/family/read.php';
require_once TAMISO_DIR . '/default/viewdata.php';
require_once TAMISO_DIR . '/default/views.php';
require_once TAMISO_DIR . '/screen/form.php';
require_once TAMISO_DIR . '/screen/read.php';
require_once TAMISO_DIR . '/family/get.php';
//require_once TAMISO_DIR . '/family/compile.php';
require_once TAMISO_DIR . '/read/dict.php';
require_once FONGO_DIR . '/core/default.php';
require_once FONGO_DIR . '/read/dict.php';
require_once ROSTO_DIR . '/read/form.php';

class tamiso_routine_create_get
{

  public bool $check_auth = true;

  protected function integrate_record(array $app, array &$record): void
  {

  }

  protected function integrate_views(array $app, array &$views): void
  {

  }

  protected function get_record(array $dict): array
  {
    return fongo_default($dict);
  }

  final public function run(array $app)
  {
    /* init */
    $moa_data = tamiso_moa_read($app);
    $family = tamiso_family_read($app);

    /* init family */
    if ($app['object']['family_level'] > 0) {
      foreach ($family['tree'] as $family_level => $member) {
        $path = $member['object_dir'] . '/get_record.php';
        if (is_file($path) && is_readable($path)) {
          $function = include $path;
          $family['tree'][$family_level]['record'] = $function($app, $member);
        } else {
          sesto_d('unable to get partent '. $path);
        }
      }
    }

    /* retrieve record from a method call or from session if validation error */
    if (false === $app['validation_error']) {
      /* load dictionary */
      $dict = tamiso_read_dict($app);
      if (empty($dict)) {
        throw new exception('alert_error_empty_dict');
      }
      $record = fongo_default($dict);
      /* fill parent related fields */
      if ($app['object']['family_level'] > 0) {
        foreach ($app['object']['family_fill_fields'] as $parent_fields => $child_fields) {
          foreach ($child_fields as $field) {
            $record[$field] = $family['tree'][$app['object']['family_parent_level']]['record'][$parent_fields];
          }
        }
      }
      $this->integrate_record($app, $record);
//      $record[$app['object']['er_field']] = $family[0]['record'][$app['object']['parent_er_field']];
    } else {
      $record = $moa_data['record'] ?? [];
    }

//    sesto_d($record, '$record');
//    sesto_d($family, '$family');
    if (empty($record)) {
      throw new exception('alert_error_empty_record');
    }
    /* regenerate csrf */
    $record['csrf'] = sesto_uuid();

    /* screen */
    if ('' === $app['action']['screen']) {
      $path = $app['procedure']['object_dir'] . '/_screen';
    } elseif ('/' !== $app['action']['screen'][0]) {
      $path = $app['procedure']['action_dir'] . '/_screen';
    } else {
      $path = $app['action']['screen'];
    }
    $screen = tamiso_screen_read($path);
    if (empty($screen)) {
      throw new exception('alert_error_empty_screen');
    }
    $screen['elements'][] = sesto_html_element('input', ['type' => 'hidden', 'name' => 'csrf', 'value' => $record['csrf']]);
    $screen['hiddens'][] = 'csrf';
    $moa_data['csrf'] = $record['csrf'];

    /* viewdata */
    $viewdata = tamiso_default_viewdata($app);
    $viewdata['screen'] = tamiso_screen_form($screen, $record, tamiso_validation_read($app), tamiso_url($app['route'], ',,,'));
    $viewdata['alert'] = $app['alert'];

    foreach ($screen['buttons'] as $button_id => $element_name) {
      $viewdata['actions']['footer'][$button_id] = $screen['elements'][$element_name] ?? [];
      $viewdata['actions']['footer'][$button_id]['attribs'] = array_merge(
        [
          'id' => 't-btn-' . $element_name,
          'name' => $element_name,
          'class' => 'toco-btn toco-btn-primary',
          'form' => 't-context-form'
        ],
        $viewdata['actions']['footer'][$button_id]['attribs']
      );
    }

    /* add buttons */
    $viewdata['actions']['footer']['save'] = sesto_html_element(
      'button',
      [
        'id' => 't_btn_save',
        'name' => 'action',
        'class' => 'toco-btn toco-btn-primary',
        'form' => 't-context-form',
        'type' => 'submit',
        'value' => 'save'
      ],
      '<i class="fas fa-save"></i> Save'
    );
    $viewdata['actions']['footer']['save_exit'] = sesto_html_element(
      'button',
      [
        'id' => 't_btn_save_exit',
        'name' => 'action',
        'class' => 'toco-btn toco-btn-primary',
        'form' => 't-context-form',
        'type' => 'submit',
        'value' => 'save_exit'
      ],
      '<i class="fas fa-save"></i> Save + <i class="fas fa-door-open"></i> Exit'
    );
    $viewdata['actions']['header']['close'] = sesto_html_element(
      'button',
      [
        'id' => 't_btn_save',
        'name' => 'action',
        'class' => 'toco-btn toco-btn-primary t-btn-goto',
        'onclick' => 't_button_click(this);',
        'value' => tamiso_url($app['route'], ',,list')
      ],
      'Close'
    );

    /* views */
    $views = tamiso_default_views($app);
    $views['page_content'] = TAMISO_VIEW_DIR . '/update_get.phtml';
    $views['list_header'] = TAMISO_VIEW_DIR . '/list_header_button_bar.phtml';
    $this->integrate_views($app, $views);

    tamiso_moa_write($app, $moa_data);

    /* render */
    sesto_view_render($views, 'layout', $viewdata);
  }

}
