<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/view/render.php';
require_once SESTO_DIR . '/html/attribs.php';
require_once SESTO_DIR . '/html/element.php';
require_once SESTO_DIR . '/html/build.php';
require_once TAMISO_DIR . '/moa/read.php';
require_once TAMISO_DIR . '/moa/write.php';

require_once TAMISO_DIR . '/actions/load.php';
require_once TAMISO_DIR . '/compile/buttons.php';
require_once TAMISO_DIR . '/family/read.php';
require_once TAMISO_DIR . '/screen/read.php';
//require_once TAMISO_DIR . '/family/compile.php';
require_once TAMISO_DIR . '/family/read.php';
require_once TAMISO_DIR . '/family/delete.php';
//require_once TAMISO_DIR . '/config/actions_read.php';
require_once TAMISO_DIR . '/default/views.php';
require_once TAMISO_DIR . '/default/viewdata.php';

//require_once TAMISO_DIR . '/bcrumb/write.php';

class tamiso_routine_list
{

  public bool $check_auth = true;

  protected function integrate_records(array $app, array &$records): void
  {

  }

  protected function get_records(array $app, array $family): array
  {
    throw new exception('get_records() not overridden');
  }

  final public function run(array $app)
  {
    /* init */
    $moa_data = tamiso_moa_read($app);
    $family = tamiso_family_read($app);

     /* screen */
    if ('' === $app['action']['screen']) {
      $path = $app['procedure']['action_dir'] . '/_screen';
    } else {
      $path = $app['action']['screen'];
    }
    $screen = tamiso_screen_read($path);
    $viewdata = tamiso_default_viewdata($app);
    $views = tamiso_default_views($app);
    $actions = tamiso_actions_load($app, $app['procedure']['action_dir']);

    /**
     * family
     */
    if ($app['object']['family_level'] > 0) {
      foreach ($family['tree'] as $family_level => $member) {
        if (!isset($family['tree'][$family_level]['parent_id'])) {
          $family['tree'][$family_level]['parent_id'] = $member['ids']['id_value'] ?? null;

          $path = $member['object_dir'] . '/get_record.php';
          if (is_file($path) && is_readable($path)) {
            $function = include $path;
            $family['tree'][$family_level]['record'] = $function($app, $member);
            $family['tree'][$family_level]['record_caption'] = $family['tree'][$family_level]['record'][$member['object']['family_caption_field']];
          } else {
            sesto_d('unable to get partent ' . $path);
          }
        }
      }
    }

    /**
     * records
     */
    $records = $this->get_records($app, $family);
    $this->integrate_records($app, $family, $records);

    /* enrich screen */
    $screen['hidden']['t-caller'] = sesto_html_element(
      'input',
      ['id' => 't-caller', 'name' => 't-caller', 'type' => 'hidden', 'value' => $app['route']['/moa']],
    );
//    $screen['hidden']['caller_caption'] = sesto_html_element(
//      'input',
//      ['id' => 'caller_caption', 'name' => 'caller_caption', 'type' => 'hidden', 'value' => $app['object']['caption']],
//    );
    $screen['hidden']['t-target'] = sesto_html_element(
      'input',
      ['id' => 't-target', 'name' => 't-target', 'type' => 'hidden', 'value' => '']);

//    sesto_d($screen, '$screen');
    /* viewdata */
    $viewdata['form_action'] = tamiso_url($app['route'], 'tamiso,implement,setids');
    $viewdata['screen'] = $screen;
    $viewdata['alert'] = $app['alert'];
    $viewdata['target'] = '';
//    $viewdata['t_type'] = '';
    $viewdata['records'] = $records;
    $viewdata['actions'] = tamiso_compile_buttons($app, $actions);

    /* views */
    $views['page_content'] = TAMISO_VIEW_DIR . '/list.phtml';
    $views['list_header'] = TAMISO_VIEW_DIR . '/list_header_button_bar.phtml';

    if ($app['object']['family_level'] > 0) {
      $views['list_family'] = TAMISO_VIEW_DIR . '/list_family.phtml';
      foreach ($family['tree'] as $family_level => $member) {
        $elements = [];
        $url = tamiso_url($app['route'], $member['url']);
        $elements[] = sesto_html_element(
          'button',
          [
            'id' => 't-btn-family_parent',
            'name' => 't-btn-family_parent',
            'class' => 'toco-btn toco-btn-primary t-btn-goto',
            'onclick' => 't_button_click(this);',
            'value' => $url
          ],
          $member['object_caption'] ?? '--'
        );
        if ($member['ids']['size'] > 1) {
          if ($member['ids']['pos'] < ($member['ids']['size'] - 1)) {
            $elements[] = sesto_html_element(
              'button',
              [
                'id' => 't-btn-family_parent',
                'name' => 't-btn-family_parent',
                'class' => 'toco-btn toco-btn-primary t-btn-goto',
                'onclick' => 't_button_click(this);',
                'value' => $url
              ],
              '<i class="fas fa-caret-left"></i>'
            );
          }
          if (0 === $member['ids']['pos']) {
            $elements[] = sesto_html_element(
              'button',
              [
                'id' => 't-btn-family_parent',
                'name' => 't-btn-family_parent',
                'class' => 'toco-btn toco-btn-primary t-btn-goto',
                'onclick' => 't_button_click(this);',
                'value' => $url
              ],
              '<i class="fas fa-caret-right"></i>'
            );
          }
        }

        $viewdata['family'][$family_level]['elements'] = $elements;
        $viewdata['family'][$family_level]['caption'] = $member['record_caption'] ?? '--';
      }
    }
    /* render */
    sesto_view_render($views, 'layout', $viewdata);
    /* reset alert */
    $moa_data['alert'] = [];
    tamiso_moa_write($app, $moa_data);
  }

}
