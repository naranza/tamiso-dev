<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/view/render.php';
require_once SESTO_DIR . '/html/element.php';
require_once SESTO_DIR . '/html/build.php';
require_once TAMISO_DIR . '/moa/read.php';
require_once TAMISO_DIR . '/moa/write.php';
require_once TAMISO_DIR . '/default/viewdata.php';
require_once TAMISO_DIR . '/default/views.php';
require_once TAMISO_DIR . '/screen/form.php';
require_once TAMISO_DIR . '/screen/read.php';
require_once TAMISO_DIR . '/read/dict.php';
require_once FONGO_DIR . '/core/default.php';
require_once FONGO_DIR . '/read/dict.php';
require_once ROSTO_DIR . '/read/form.php';

class tamiso_routine_update_get
{

  public bool $check_auth = true;

  protected function integrate_record(array $app, array &$record): void
  {

  }

  protected function integrate_views(array $app, array &$views): void
  {

  }

  protected function get_record(array $app, string $id): array
  {
    throw new exception('get_records() not overridden');
  }

  final public function run(array $app)
  {
    /* init */
    $moa_data = tamiso_moa_read($app);

    /* retrieve record */
    if (false === $app['validation_error']) {
      $record = $this->get_record($app, $moa_data['caller']['ids']['id_value']);
      $this->integrate_record($app, $record);
    } else {
      $record = $moa_data['record'] ?? [];
    }
    if (empty($record)) {
      throw new exception('alert_error_empty_record');
    }
    /* regenerate csrf */
    $record['csrf'] = sesto_uuid();

    /* load dictionary */
    $dict = tamiso_read_dict($app);
    if (empty($dict)) {
      throw new exception('alert_error_empty_dict');
    }

    /* screen */
    $screen = tamiso_screen_read($app);

    $screen['elements'][] = sesto_html_element('input', ['type' => 'hidden', 'name' => 'csrf', 'value' => $record['csrf']]);
    $screen['hiddens'][] = 'csrf';
    $moa_data['csrf'] = $record['csrf'];

    /* viewdata */
    $viewdata = tamiso_default_viewdata($app);
    $viewdata['screen'] = tamiso_screen_form($screen, $record, tamiso_validation_read($app), tamiso_url($app['route'], ',,,'));
    $viewdata['alert'] = $app['alert'];

    foreach ($screen['buttons'] as $button_id => $element_name) {
      $viewdata['actions']['footer'][$button_id] = $screen['elements'][$element_name] ?? [];
      $viewdata['actions']['footer'][$button_id]['attribs'] = array_merge(
        [
          'id' => 't-btn-' . $element_name,
          'name' => $element_name,
          'class' => 'toco-btn toco-btn-primary',
          'form' => 't-context-form'
        ],
        $viewdata['actions']['footer'][$button_id]['attribs']
      );
    }

    /* add buttons */
    $last_label = (1 === count($moa_data['caller']['ids'])) ? '<i class="fas fa-save"></i> Save + <i class="fas fa-door-open"></i> Exit' : '<i class="fas fa-save"></i> Save + <i class="fas fa-forward"></i> Next';
    $viewdata['actions']['footer']['save'] = sesto_html_element(
      'button',
      [
        'id' => 't_btn_save',
        'name' => 'action',
        'class' => 'toco-btn toco-btn-primary',
        'form' => 't-context-form',
        'type' => 'submit',
        'value' => 'save'
      ],
      '<i class="fas fa-save"></i> Save'
    );
    $viewdata['actions']['footer']['save_exit'] = sesto_html_element(
      'button',
      [
        'id' => 't_btn_save_next',
        'name' => 'action',
        'class' => 'toco-btn toco-btn-primary',
        'form' => 't-context-form',
        'type' => 'submit',
        'value' => 'save_next'
      ],
      $last_label
    );
    $viewdata['actions']['header']['close'] = sesto_html_element(
      'button',
      [
        'id' => 't_btn_save',
        'name' => 'action',
        'class' => 'toco-btn toco-btn-primary t-btn-goto',
        'onclick' => 't_button_click(this);',
        'value' => tamiso_url($app['route'], ',,list')
      ],
      '<i class="fas fa-door-open"></i> Close'
    );

    /* views */
    $views = tamiso_default_views($app);
    $views['page_content'] = TAMISO_VIEW_DIR . '/update_get.phtml';
    $views['list_header'] = TAMISO_VIEW_DIR . '/list_header_button_bar.phtml';
    $this->integrate_views($app, $views);

    tamiso_moa_write($app, $moa_data);

    /* render */
    sesto_view_render($views, 'layout', $viewdata);
  }

}
