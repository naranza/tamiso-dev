<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/read/dict.php';
require_once TAMISO_DIR . '/moa/read.php';
require_once TAMISO_DIR . '/moa/write.php';
require_once TAMISO_DIR . '/moa/delete.php';
require_once TAMISO_DIR . '/ids/next.php';
require_once TAMISO_DIR . '/ids/is_last.php';
require_once TAMISO_DIR . '/filter/user_input.php';
require_once FONGO_DIR . '/core/process.php';
require_once FONGO_DIR . '/read/dict.php';

class tamiso_routine_form_post
{

  public bool $check_auth = true;

  public function success(array $app, array $process_result): array
  {
    throw new exception('success() not overridden');
  }

  public function run(array $app)
  {
    /* init */
    $moa_data = tamiso_moa_read($app);
    $context = [
      'csrf' => $moa_data['csrf']
    ];

    /* load dictionary */
    $dict = tamiso_read_dict($app);
    if (empty($dict)) {
      throw new exception('alert_error_empty_dict');
    }

    /* validate */
    $result = fongo_process($dict, $app['http_post'], $context, FONGO_ALL);

//    sesto_d($app['http_post'], 'http_post');
//    sesto_d($result, '$result');
//    sesto_d($dict, '$dict');
//    sesto_d($context, '$context');
//    sesto_d($moa_data, '$moa_data');
//    sesto_d($app, 'app');
//    die;

    $validation = $result[0] ?? [1];
    $processed = $result[1] ?? [];
    $missed = $result[2] ?? [];
    $validated = $result[3] ?? [];
    $garbage = $result[4] ?? [];
    $empty = $result[5] ?? [];
    $codes = $result[6] ?? [];

//    sesto_d($validation, '$validation');
//    sesto_d($processed, '$processed');
//    sesto_d($missed, '$missed');
//    sesto_d($validated, '$validated');
//    sesto_d($garbage, '$garbage');
//    sesto_d($empty, '$empty');
//    sesto_d($codes, '$codes');
//    sesto_d($context, '$context');
//    sesto_d($moa_data, '$moa_data');
//    die;
    $alerts = [];
    $redirect_url = ',,,';
    $redirect_params = [];
    $end_procedure = false;

    if (!empty($validation)) {
      $alerts[] = ['warning', 'alert_validation_error'];
      $redirect_params['t-ve'] = 1;
    } else {
      try {
        list($alert_type, $alert_message, $redirect_url, $redirect_params) = $this->success($app, $result);
        $alerts[] = [$alert_type, $alert_message];
        $end_procedure = 'success' == $alert_type;
      } catch (throwable $ex) {
        $alerts[] = ['danger', $ex->getmessage()];
        $redirect_params['t-ve'] = 1;
        sesto_d($ex);
        die;
      }
    }

    if ($end_procedure) {
      tamiso_moa_delete($app);
    } else {
      $moa_data['record'] = $processed;
      $moa_data['validation'] = $validation;
      $moa_data['alert'] = $alerts;
      tamiso_moa_write($app, $moa_data);
    }
    tamiso_alert_write($alerts);

    /* redirect */
    sesto_http_redirect(tamiso_url($app['route'], $redirect_url, $redirect_params));
  }

}
