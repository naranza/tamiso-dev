<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/routine/form_get.php';

class tamiso_procedure extends tamiso_routine_form_get
{
  public bool $check_auth = false;

  protected function integrate_views(array $app, array &$views): void
  {
    unset($views['page_header']);
  }

}