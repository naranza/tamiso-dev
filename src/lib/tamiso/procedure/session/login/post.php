<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/routine/form_post.php';
require_once TAMISO_DIR . '/user/read_by_username.php';

class tamiso_procedure extends tamiso_routine_form_post
{

  public bool $check_auth = false;

  public function success(array $app, array $process_result): array
  {

    /* check the credential */
    $user = tamiso_user_read_by_username(sesto_resource('tamiso_db'), $process_result[1]['username'] ?? uniqid());
    $verified = password_verify($process_result[1]['password'] ?? uniqid(), $user['password'] ?? uniqid());
    if ($verified) {
      $_SESSION = [];
      unset($user['password']);
      $_SESSION['tamiso']['user'] = $user;
      $alert_type = 'success';
      $alert_message = 'alert_login_success';
      $redirect_url = 'tamiso/dashboard/index';
      $redirect_params = [];
    } else {
      $alert_type = 'warning';
      $alert_message = 'alert_login_failed';
      $redirect_url = ',,login';
      $redirect_params['t-ve'] = 1;
    }
    return [$alert_type, $alert_message, $redirect_url, $redirect_params];
  }

}
