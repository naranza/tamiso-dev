<?php

//

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/struct/ids.php';
require_once TAMISO_DIR . '/filter/user_input.php';
require_once TAMISO_DIR . '/moa/session.php';
require_once TAMISO_DIR . '/ids/create.php';
require_once TAMISO_DIR . '/actions/load.php';
//require_once TAMISO_DIR . '/family/read.php';
require_once TAMISO_DIR . '/family/add.php';
require_once SESTO_DIR . '/util/uuid.php';
require_once SESTO_DIR . '/http/redirect.php';

class tamiso_procedure
{

  public function run(array $app)
  {
    if ('POST' !== $_SERVER['REQUEST_METHOD']) {
      /* need to decide where */
      return;
    }

    /* read input */
    $input = tamiso_filter_user_input($_POST);
    $ids = $input['t-ids'] ?? [];
    $caller = $input['t-caller'] ?? '';
    $action = $input['t-action'] ?? uniqid();

    /* load caller data: actions, config etc. */
    $caller_route = tamiso_route_resolve(sesto_env('route_base_dir'), $caller);
    sesto_d($action, '$action');
    sesto_d($caller_route, '$caller_object');
    $caller_module_root_dir = $app['tamiso']['module'][$caller_route['module']] ?? null;
    if (null === $caller_module_root_dir) {
      throw new exception('Uknown module ' . $caller_route['module']);
    }
    list($caller_module_dir, $caller_object_dir, $caller_action_dir) = tamiso_moa_dirs(
      $caller_module_root_dir,
      $caller_route['object'],
      $caller_route['action']);
    $caller_actions = tamiso_actions_load($app, $caller_action_dir);
    $caller_object = tamiso_object_load($app, $caller_object_dir);
    sesto_d($caller_actions, '$caller_actions');
    sesto_d($caller_object, '$caller_object');
    /* calc target url */
    $target = tamiso_url($caller_route, $caller_actions[$action]['target_route'], $caller_actions[$action]['params'] ?? []);
    sesto_d($target, '$target');
//    die;

//    die;
    $pos = 0;
    $params = [];
//    if (empty($ids) && false === strpos($target, 'create')) {
    if (empty($ids)) {
      tamiso_alert_write([['warning', 'Please select at least one item']]);
      $destination = $caller;
    } else {
      $destination = $target;
      $session_record = [
        'url' => $caller,
        'route' => $caller_route,
        'module_dir' => $caller_module_dir,
        'object_dir' => $caller_object_dir,
        'action_dir' => $caller_action_dir,
        'object' => $caller_object,
        'object_caption' => $caller_object['caption'] ?? '',
        'ids' => tamiso_ids_create($ids, 0)
      ];
//      sesto_d($caller_actions[$action], 'action');
//      $family_name = $caller_object['family_name'] ?? '';
//      $family_type = $caller_object['family_type'] ?? 'parent';
      if ('' !== $caller_object['family_name'] && $caller_actions[$action]['set_family']) {
        tamiso_family_add($caller_object['family_name'], $caller_object['family_level'], $session_record);
      } else {
        $_SESSION['tamiso'][$target]['caller'] = $session_record;
      }
    }
//    sesto_d($app['object']);
//    sesto_dump($input, '$input');
//    sesto_dump($pos, '$pos');
//    sesto_dump($app['route'], 'route');
//    sesto_dump($caller_object, '$caller_object');
//    sesto_dump($caller_actions, '$caller_actions');
//    sesto_dump($target, '$target');
//    sesto_dump($destination, '$destination');
//    sesto_dump($params, '$params');
//    sesto_dump(tamiso_moa_session(tamiso_moa_from_path($caller)), 'moa caller');
//    sesto_dump_session();
//    die;
    sesto_http_redirect(tamiso_url($app['route'], $destination, $params));
  }

}
