<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_validation_write(array $app, array $data): void
{
  if (isset($app['route']['/moa'])) {
    $_SESSION['tamiso'][$app['route']['/moa']]['validation'] = $data;
  }
}
