<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_default_viewdata(array $app): array
{
  $data = [];
  $data['route'] = $app['route'];
  $data['tid'] = $app['tid'] ?? '';
  $data['t_session_moa_mo'] = $app['route']['dirname'];

  /* breadcrumb */
  $data['t_bcrumb'] = [
    [
      'label' => $app['module']['caption'] ?? '--'
    ],
    [
      'label' => $app['object']['caption'] ?? '--'
    ],
    [
      'label' => $app['action']['caption'] ?? '--'
    ],
  ];

  return $data;
}