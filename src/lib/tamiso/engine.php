<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

//require_once SESTO_DIR . '/core/config_read.php';
//require_once SESTO_DIR . '/dev/dump.php';
//require_once SESTO_DIR . '/dev/var_dump.php';
//require_once SESTO_DIR . '/dev/dump_session.php';
//require_once SESTO_DIR . '/util/uuid.php';
//require_once SESTO_DIR . '/util/is_file_readable.php';
//require_once SESTO_DIR . '/http/redirect.php';
//require_once SESTO_DIR . '/http/is_post.php';
//require_once SESTO_DIR . '/http/is_get.php';
//require_once TAMISO_DIR . '/alert/read.php';
//require_once TAMISO_DIR . '/alert/write.php';
//require_once TAMISO_DIR . '/validation/read.php';
//require_once TAMISO_DIR . '/validation/write.php';
//require_once TAMISO_DIR . '/core/include.php';
//require_once TAMISO_DIR . '/filter/user_input.php';
//require_once TAMISO_DIR . '/route/resolve.php';
//require_once TAMISO_DIR . '/module/load.php';
//require_once TAMISO_DIR . '/object/load.php';
//require_once TAMISO_DIR . '/action/load.php';
//require_once TAMISO_DIR . '/url.php';
//require_once TAMISO_DIR . '/moa/dirs.php';
//require_once TAMISO_DIR . '/read/actions.php';
//require_once FONGO_DIR . '/core/path.php';

function tamiso_engine(array $config, array $args): void
{
  /* enable output buffer */
  ob_start();

  $app = [];
  $app['tamiso']['module'] = $config['module'] ?? [];

  /* load config, add module tamiso and parse */
  $app['tamiso']['module']['tamiso'] = TAMISO_DIR;

  /* route */
  $app['route'] = tamiso_route_resolve(sesto_env('route_base_dir'));

  fongo_path('tamiso', TAMISO_DIR . '/fongo');

  /* module - object - action */
  $app['tamiso']['module_root_dir'] = $app['tamiso']['module'][$app['route']['module']] ?? null;
  if (null === $app['tamiso']['module_root_dir']) {
    throw new exception('Uknown module ' . $app['route']['module']);
  }

  list(
    $app['procedure']['module_dir'],
    $app['procedure']['object_dir'],
    $app['procedure']['action_dir'],
    ) = tamiso_moa_dirs(
      $app['tamiso']['module_root_dir'],
      $app['route']['object'],
      $app['route']['action']);


//  $app['procedure']['dicts'] = ['t_object::_data/dict/' . $app['route']['action']];
  $app['procedure']['csrf'] = true;
  $app['procedure']['auto_screen'] = true;
  $app['procedure']['auto_dict'] = true;

  /* register on fongo the paths for module object and action */
  fongo_path('module', $app['procedure']['module_dir']);
  fongo_path('object', $app['procedure']['object_dir']);
  fongo_path('action', $app['procedure']['action_dir']);

  $app['module'] = tamiso_module_load($app, $app['procedure']['module_dir']);
  $app['object'] = tamiso_object_load($app, $app['procedure']['object_dir']);
  $app['action'] = tamiso_action_load($app, $app['procedure']['action_dir']);

  /* exec */
  if (sesto_http_is_get()) {
    $procedure_run_path = $app['procedure']['action_dir'] . '/get.php';
    $app['csrf'] = sesto_uuid();
  } elseif (sesto_http_is_post()) {
    $procedure_run_path = $app['procedure']['action_dir'] . '/post.php';
    $app['csrf'] = '';
  } else {
    throw new exception("unknown http verb");
  }

  /* initmes */
  foreach (array_merge($config['initme'], [$app['tamiso']['module_root_dir']])  as $initme) {
    $initme = $initme . '/initme.php';
    if (is_file($initme) && is_readable($initme)) {
      require_once $initme;
    }
  }
  /* session management */
  session_start();

  /* read post */
  $app['http_post'] = tamiso_filter_user_input($_POST);
  $app['http_get'] = tamiso_filter_user_input($_GET);
  $app['validation_error'] = (bool) ($app['http_get']['t-ve'] ?? false);
  /* read and clear alerts */
  $app['alert'] = tamiso_alert_read();
  tamiso_alert_write([]);

  if (sesto_is_file_readable($procedure_run_path)) {
    tamiso_include($procedure_run_path, $app);
  } else {
    throw new exception($procedure_run_path . ' is not readable');
  }

//  sesto_d(get_defined_constants(true)['user']);
//  sesto_d($app);
//  die;
  $procedure = new tamiso_procedure();
  if (!property_exists($procedure, 'check_auth') || $procedure->check_auth) {
    if (!isset($_SESSION['tamiso']['user'])) {
      throw new exception('unauthorized', 401);
    }
  }
  $procedure->run($app);
}
