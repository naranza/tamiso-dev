<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

return [
  'function' => function ($value, $context) { return $value === ($context ?? uniqid()); },
  'message' => 'csrf check failed',
  'args' => ['context::csrf']
];
