<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

return [
  'require' => SESTO_DIR . '/rule/uuidv4.php',
  'function' => 'sesto_rule_uuidv4',
  ''
];
