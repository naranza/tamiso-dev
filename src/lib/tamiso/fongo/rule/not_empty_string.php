<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

return [
  'function' => function ($value) { return (bool) preg_match('/\S/', $value); },
  'message' => 'Cannot be empty'
];
