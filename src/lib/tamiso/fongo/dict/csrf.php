<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

return [
  'csrf' => [
    'rule' => ['tamiso::rule/uuidv4', 'tamiso::rule/csrf_match']
  ]
];
