<?php
/* =============================================================================
 * Naranza Bateo, Copyright (c) Andrea Davanzo, License GNU GPL v3.0, bateo.dev
 * ========================================================================== */

$config = [];

$config['recursive'] = true;
$config['testcase_summary_level'] = 1;
